package com.nu.art.software.bug.test.recyclerscrolltoposition;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class RecyclerTestActivity
		extends AppCompatActivity {

	private RecyclerView recyclerView;

	private GridLayoutManager layout;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_recycler_test);
		//		setContentView(R.layout.activity_recycler_test_with_bug);
		recyclerView = (RecyclerView) findViewById(R.id.RecyclerView);
		layout = new GridLayoutManager(this, 3);
		recyclerView.setLayoutManager(layout);
		recyclerView.setAdapter(new TestAdapter());
		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				layout.scrollToPosition(100);
			}
		}, 3000);
	}

	private class TestAdapter
			extends Adapter<HolderType> {

		@Override
		public HolderType onCreateViewHolder(ViewGroup parent, int viewType) {
			return new HolderType(getLayoutInflater().inflate(R.layout.list_node__recycler_example_int, parent, false));
		}

		@Override
		public void onBindViewHolder(HolderType holder, int position) {
			holder.render(position);
		}

		@Override
		public int getItemCount() {
			return 1000;
		}
	}

	private class HolderType
			extends ViewHolder {

		private final TextView label;

		public HolderType(View itemView) {
			super(itemView);
			label = (TextView) itemView.findViewById(R.id.ExampleLabel);
		}

		public final void render(Integer num) {
			label.setText(num + "");
		}
	}
}
